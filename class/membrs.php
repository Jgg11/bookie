<?php

class membrs{

	private $conn;

	function __construct(){
		error_reporting(E_ALL);
		ini_set('display_errors', 1);
		$this->conn = new PDO('sqlite:SQ23154862153515215BAI/data.db');


	}




	//Notkun: validation(user,password)
	//Fyrir: ekkert
	//Eftir: Skráir notenda inn ef að user/pass er rétt, annars gefur það villumeldingu.
	function validation($usern, $pwrd){

		$query = "SELECT ID FROM accounts WHERE username = :username AND password = :password LIMIT 1";
		
		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':username' => $usern,
  			':password' => $pwrd
   			));
			
			if($result = $stam->fetch()){

				//$stam->close();
				$_SESSION['status'] =  'authorized';
				$_SESSION['id'] = $result['ID'];
				$_SESSION['name'] = $usern;
				$_SESSION['lastAction'] = time();
				
				
			}
			else return "Wrong password or username!";
		}
		else return "Sumtang wong";

	}
	//Notkun: endValidation()
	//Fyrir: Ekkert
	//Eftir: Búið er að eyða session fyrir notanda og notandi skráður út.
	function endValidation(){

		if(isset($_SESSION['status'])){
			foreach($_SESSION as $value){
				unset($value);
			}

		}

		if(isset($_COOKIE[session_name()])) setcookie(session_name(), "", time()-10000);
		session_destroy();
		

	}
	//Notkun: registration(user,password,email)
	//Fyrir: ekkert
	//Eftir: Ný notandi er skráður með user, pwrd og email.
	function registration($usern, $pwrd, $email){

		$query = "INSERT INTO accounts(username, password, email, level, favorites) VALUES(:username, :password, :email, 0, :favorites)";
		$fav = [];

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':username' => $usern,
  			':password' => $pwrd,
  			':email' => $email,
  			':favorites' => serialize($fav)
   			));
   		}

	}
	//Notkun: addToken()
	//Fyrir: ekkert
	//Eftir: Búið er að skrá niður tóken sem er notað fyrir password reset.
	function addToken($id, $token, $time){

		$query = "INSERT INTO resetpwd(token, id, time) VALUES(:token, :id, :time)";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':token' => $token,
  			':id' => $id,
  			':time' => $time
   			));
   			
   		}

	}
	//Notkun: addScore()
	//Fyrir: ekkert
	//Eftir: Bætir við nýrri einkunn
	function selectBookBetweenId($idStart, $idEnd){

		$query = "SELECT * FROM books ORDER BY name limit :idStart, :idEnd";
	
		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
	  			':idStart' => $idStart,
	  			':idEnd' => $idEnd
   			));
   			$result = $stam->fetchAll();
   			return $result;
   			
   		}

	}

	function bookSearchBetweenId($q, $idStart, $idEnd){
	
		$query = "SELECT * FROM books WHERE name LIKE :q OR author LIKE :q ORDER BY name limit :idStart, :idEnd";
	
		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
				':q' => $q,
	  			':idStart' => $idStart,
	  			':idEnd' => $idEnd
   			));
   			$result = $stam->fetchAll();
   			return $result;
   			
   		}

	}
	//Notkun: getBookById($id)
	//Fyrir: $id er int breyta
	//Eftir: Skilar bók sem hefur id = $id
	function getBookById($id){
		$query = "SELECT * FROM books WHERE id = :id";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
				':id' => $id
				));
			$result = $stam->fetch();
			
		   	return $result;
		   }
	}
	//Notkun: getScore()
	//Fyrir: ekkert
	//Eftir: skilar lista raðað eftir hæsta scori
	function getScore(){

		$query = "SELECT * FROM astScore ORDER BY score DESC";


		if($stam = $this->conn->prepare($query)){
			$stam->execute();
			$result = $stam->fetchAll();
			
		   	return $result;

		}


	}

	function createFav($user){
		$fav = [];
	}

	function addToFav($user, $book){
		$query = "SELECT * FROM accounts WHERE id = :user";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':user' => $user,
   			));
			$result = $stam->fetch();
		}

		$fav = unserialize($result['favorites']);
		array_push($fav, $book);

		$fav = serialize($fav);

		$query = "UPDATE accounts SET favorites = :fav WHERE id = :user";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':fav' => $fav,
  			':user' => $user
   			));
   			
   		}

	}

	function getFavId($user){
		$query = "SELECT * FROM accounts WHERE id = :user";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':user' => $user,
   			));
			$result = $stam->fetch();
			return unserialize ($result['favorites']);
		}

	}

	function addBook($name, $author, $year, $summary){
		$query = "INSERT INTO books(name, author, year, summary) VALUES(:name, :author, :year, :summary)";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':name' => $name,
  			':author' => $author,
  			':year' => $year,
  			':summary' => $summary
   			));
		}
	}

	//Notkun: getScoreById()
	//Fyrir: ekkert
	//Eftir: skilar scori sem það id hefur
	function getScoreById($id){

	$query = "SELECT * FROM astScore WHERE id = :id ORDER BY score DESC";


		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':id' => $id,
   			));
			$result = $stam->fetchAll();
			
		   	return $result;

		}

	}

	function getUser($id){
		$query = "SELECT * FROM accounts WHERE id = :id";


		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':id' => $id
   			));
			$result = $stam->fetch();
			
		   	return $result;

		}
	}

	//Notkun: newPwd()
	//Fyrir: ekkert
	//Eftir: Búið er að endurstill password með nýju passwordi.
	function newPwd($id, $password){

		$query = "UPDATE accounts SET password = :password WHERE id = :id";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':id' => $id,
  			':password' => $password
   			));
   			
   		}

	}
	//Notkun: removeToken($token)
	//Fyrir: ekkert
	//Eftir: Búið er að eyða $token úr gangagrún;
	function removeToken($token){

		$query = "DELETE FROM resetpwd WHERE token = :token";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':token' => $token,
   			));
		}
	}	
	//Notkun: isLogedIn()
	//Fyrir: ekkert
	//Eftir: skilar true ef að notandi er logged in, annars false
	function isLogedIn(){
		

		if(isset($_SESSION['status']) &&$_SESSION['status'] == 'authorized'){

			return true;
		}
		else return false;
	}
	//Notkun: checkName()
	//Fyrir: ekkert
	//Eftir: skilar true ef að notendanafn er laust
	function checkName($name){

		$query = "SELECT * FROM accounts WHERE username = :username";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':username' => $name,
   			));
			
			if($stam->fetch()){
				//$stam->close();
				return true;
			}
			
		}
		else return "Sumtang wong";

	}

	function checkBook($name){

		$query = "SELECT * FROM books WHERE name = :name";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':name' => $name,
   			));
			
			if($stam->fetch()){
				//$stam->close();
				return true;
			}
			
		}
		else return "Sumtang wong";

	}
	//Notkun: checkLastAction()
	//Fyrir: ekkert
	//Eftir: Skilar hvenær síðast far hreyfing hjá notanda.
	function checkLastAction(){
		if(time() > ($_SESSION['lastAction']+600)){
			return true;
		}
	}
	//Notkun: updateAction()
	//Fyrir: ekkert
	//Eftir: Búið er að uppfæra hvenær var síðasta hreyfing hjá notanda.
	function updateAction(){
		$_SESSION['lastAction'] = time();
	}
	//Notkun: checkEmail(token)
	//Fyrir:ekkert
	//Eftir:skilar true ef að email er laust
	function checkEmail($email){

		$query = "SELECT * FROM accounts WHERE email = :email";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':email' => $email,
   			));
			
			if($stam->fetch()){
				//$stam->close();
				return true;
			}
			
		}
		else return "Sumtang wong";

	}
	//Notkun: checkToken(token)
	//Fyrir: $token er strengur.
	//Eftir: Skilar hvort $token sé til í gangagrunninum.
	function checkToken($token){
		$query = "SELECT * FROM resetpwd WHERE token = :token";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':token' => $token,
   			));
			
			if($stam->fetch()){
				//$stam->close();
				return true;
			}
			
		}

	}
	//Notkun: getIdByEmail()
	//Fyrir: ekkert
	//Eftir: skilar því id sem að er tengt við emailið.
	function getIdByEmail($email){

			$query = "SELECT id FROM accounts WHERE email = :email";

			if($stam = $this->conn->prepare($query)){
				$stam->execute(array(
	  			':email' => $email,
	   			));

	   			$result = $stam->fetch();
	   			return $result['ID'];
	   		}
			
	}
	//Notkun: getIdByToken()
	//Fyrir: $token er strengur.
	//Eftir: Skilar ID notenda.
	function getIdByToken($token){
		$query = "SELECT id FROM resetpwd WHERE token = :token";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
  			':token' => $token,
   			));

   			$result = $stam->fetch();
   			return $result['id'];
   		}
	}
	//Notkun: getCategories()
	//Fyrir: ekkert
	//Eftir: skilar öllum flokkum
	function getCategories(){
		$query = "SELECT * FROM categories";

		if($stam = $this->conn->prepare($query)){
			$stam->execute();
			$result = $stam->fetchAll();
			
		   	return $result;

		
		}
	}
	//Notkun: getPosts($id)
	//Fyrir: $id er tala.
	//Eftir: Skilar pósti frá gangagrunni.
	function getPosts($id){

		$query = "SELECT * FROM posts WHERE post_cat = :id";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
				':id' => $id,
				));
			$result = $stam->fetchAll();
		   	return $result;

		
		}
	}
	//Notkun: getPosts($post_id, $post_cat)
	//Fyrir: $post_id og $post_cat eru tölur.
	//Eftir: Skilar pósti frá gagnagrunni.
	function getPost($post_id, $post_cat){

		$query = "SELECT * FROM posts WHERE post_id = :post_id AND post_cat = :post_cat";

		if($stam = $this->conn->prepare($query)){
			$stam->execute(array(
				':post_id' => $post_id,
				':post_cat' => $post_cat
				));
			$result = $stam->fetch();
		   	return $result;

		
		}
	}



}

?>