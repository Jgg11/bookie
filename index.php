<?php
	
	header('Content-Type: text/html; charset=utf-8');
	
	session_start();
	require_once "class/membrs.php";
	$myMembrs = new membrs();

	$method = $_SERVER["REQUEST_METHOD"];

	//$errors = []; Virkar ekki með file zilla

	if ($method === "POST")
	{
	
		if(isset($_POST["login"])){
			
			if(!empty($_POST["username"]) && !empty($_POST["password"])){
				if(!$errors["login"] = $myMembrs->validation($_POST['username'], md5($_POST['password']))){
					header("Location: ?Home"); 
				}
				
			}

		}

		if(isset($_POST["register"])){
			if(!empty($_POST['username']) && !empty($_POST['password1']) && !empty($_POST['password2']) && !empty($_POST['email'])){
				$pwdRight = $_POST['password1'] != $_POST['password2'];
				
		
				if(!$myMembrs->checkName($_POST['username']) && !$myMembrs->checkEmail($_POST['email']) && !$pwdRight){

					$myMembrs->registration($_POST['username'], md5($_POST['password1']), $_POST['email']);
					$errors["login"] = "Thank you for registering";
					header("location: ?Login");
					
				}
			}
		}
		if(isset($_POST["search"])){
			header("location: ?Books&q=". $_POST['searchInp']);
		}

		if(isset($_POST["bookCreate"])){
			echo "<p>lol<p>";
			if(!empty($_POST['post_name']) && !empty($_POST['post_author']) && !empty($_POST['post_year']) && !empty($_POST['post_summary'])){
				if(!$myMembrs->checkBook($_POST['post_name'])){
					$myMembrs->addBook($_POST['post_name'], $_POST['post_author'], $_POST['post_year'],$_POST['post_summary']);
					header("location: ?Books");
				}

			}
		}
		
	}


	include('views/header.php');
	include('views/navbar.php');


	if(isset($_GET["Logout"])){
		$myMembrs->endValidation();
		header("Location: ?Home");
	}
	else if(isset($_GET['Register'])){
		include('views/register.php');
	}
	else if (isset($_GET['Books'])){
		include('views/books.php');
	}
	else if (isset($_GET['Book'])){
		include('views/book.php');
	}
	else if (isset($_GET['Home'])){
		include('views/home.php');
	}
	else if (isset($_GET['Authors'])){
		include('views/authors.php');
	}
	else if (isset($_GET['Contact'])){
		include('views/contact.php');
	}
	else if (isset($_GET['bookcreate'])){
		include('views/bookcreate.php');
	}
	else if(isset($_GET['User'])){
		include('views/user.php');
	}
	else{
		include('views/home.php');
	}

	
	include('views/footer.php');
	include('views/hidden.php');

	
?>