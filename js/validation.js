$(document).ready(function () {

    $("#loginBtn").click(function() {
        $("#loginForm").css("display", "block");
        $("#black-div").css("display", "block");
    });

     $("#black-div").click(function() {
        $("#loginForm").css("display", "none");
        $("#black-div").css("display", "none");
     });

    $('#login-form').validate({
        
        rules: {
            username: {
                minlength: 3,
                required: true
            },
            password: {
                required: true,
            },

        },
        
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {

            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function(error, element){}
    
    });

    $('#register-form').validate({

        rules: {
            username: {
                minlength: 3,
                required: true
            },
            password1: {
                required: true
            },
            password2: {
                equalTo: "#password1"
            },
            email: {
                required: true,
                email: true
            }


        },

        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {

            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function(error, element){
             error.appendTo( $(element).closest('.error-label'));
        }
    });
});