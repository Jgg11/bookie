<div class= "contact">
 <h1>Contact Us!</h1>
        <form method="POST" action="contact-form-submission.php" class="form-horizontal">
            <div class="control-group">
                <label class="control-label" for="input1">Name</label>
                <div class="controls">
                    <input type="text" name="contact_name" id="input1" placeholder="Jón Jónsson" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input2">Email Address</label>
                <div class="controls">
                    <input type="text" name="contact_email" id="input2" placeholder="jon@jonsson.is" pattern="[^ @]*@[^ @]*" required>
                </div>
            </div>
            <div class="control-group">

                <label class="control-label" for="input3">Message</label>
                <div class="controls">
                    <textarea name="contact_message" id="input3" rows="10" columns="10" cols="40" class="span5" placeholder="Skilaboðin sem þú vilt senda okkur" required> </textarea>
                </div>
            </div>
            <div class="form-actions">
                <input type="hidden" name="save" value="contact">
                <button type="submit" class="btn btn-primary" >Send</button>

            </div>
        </form>
</div>