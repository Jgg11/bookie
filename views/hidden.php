<div id="black-div">
</div>
<div class="form-div " id="loginForm">
    <form class="form-horizontal" role="form" action="" method="POST" id="login-form">
        <div class="form-group">
            <div class="col-xs-12">
                <h1>Welcome</h1>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label  for="inputName">Username</label>
                <input type="text" class="form-control" id="inputName" name="username">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label  for="inputPassword">Password</label>
                <input type="password" class="form-control" id="inputPassword" name="password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-6">
                <p>Not a member? <a href="?Register">Register</a></p>
            </div>
            <div class="col-xs-6">
                <p class="text-right"><a class="text-right" href="?Register">Forgotten Password</a></p>
            </div>
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary" name="login">Login</button>
                <?php
                     if(isset($errors["login"])){
                         echo "<label>".$errors["login"]."</label>";
                     }
                 ?>
            </div>
        </div>
    </form>
</div>
