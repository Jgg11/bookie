<div class="form-div">
    <form class="form-horizontal" role="form" action="" method="POST" id="register-form">
        <div class="form-group">
            <div class="col-xs-12">
                <h1>Register</h1>
            </div>
        </div>
        <div class="form-group"> 
            <div class="col-xs-12">
                 <label  for="inputName">UserName</label><label class=".error-label"></label>
                <input type="text" class="form-control" name="username" id="inputName">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label  for="password1">Password</label><label class=".error-label"></label>
                <input type="password" class="form-control" name="password1" id="password1">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label  for="password2">Retype password</label><label class=".error-label"></label>
                <input type="password" class="form-control" name="password2" id="password2">
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label  for="inputEmail">Email</label><label class=".error-label"></label>
                <input type="email" class="form-control" name="email" id="inputEmail">
            </div>
        </div>
       
        <div class="form-group">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary" name="register">Register</button>
                <?php
                     if(isset($errors["login"])){
                         echo "<label>".$errors["login"]."</label>";
                     }
                 ?>
            </div>
        </div>
    </form>
</div>