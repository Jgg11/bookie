<div id="search-form-div">
<form role="form"  action="" method="post">
	<div class="form-group">
		<label for="bookname">Book name:</label>
		<input type="text" class="form-control" name="post_name" id="post_name" placeholder="John Doe Autobiography" required>
	</div>
	<div class="form-group">
		<label for="author">Author:</label>
		<input type="text" class="form-control" name="post_author" id="post_author" placeholder="John Doe" required>
	</div>
		<div class="form-group">
		<label for="year">Released:</label>
		<input type="number" class="form-control" name="post_year" id="post_year" min="0" max="2030" maxlength="4" placeholder="2015" required>
	</div>
	<div class="form-group">
		<label for="summary">Summary:</label>
		<textarea required rows="3" class="form-control" name="post_summary" id="post_summary" maxlength="300" placeholder="The autobiography about the amazing life of John Doe"></textarea>
	</div>
	<button type="submit" class="btn btn-default" name="bookCreate">Submit</button>
</form>
</div>