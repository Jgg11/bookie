
<div class ="navbar navbar-default navbar-fixed-top">
  <div class="container">
  	<a href="#" class="navbar-brand"></a>
  	<button class="navbar-toggle" data-toggle="collapse" data-target =".navHeaderCollapse">
  		<span class="icon-bar"></span>
  		<span class="icon-bar"></span>
  	</button>
    <div class ="collapse navbar-collapse navHeaderCollapse">
    	<ul class="nav navbar-nav navbar-right">
    		<li><a href="?Home">Home</a></li>
        <li><a href="?Books">Books</a></li>
    		<li><a href="?Authors">Authors</a></li>
    		<li><a href="?Contact">Contact</a></li>
        <?php
          if($myMembrs->isLogedIn()){
            echo "<li class='dropdown'>
                  <a href='?Drowpdown' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>".$_SESSION['name']."<span class='caret'></span></a>
                  <ul class='dropdown-menu' role='menu'>
                    <li><a href='?User=".$_SESSION['id']."'>Profile</a></li>
                    <li><a href='?bookcreate'>Add a book</a></li>
                    <li><a href='?Logout'>Logout</a></li>
                  </ul>
                </li>";
          }
          else{
            echo "<li><a id='loginBtn'>Login</a></li>";
          }
        ?>	
        <div class="col-sm-3 col-md-3">
        <form class="navbar-form" role="search">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
            <div class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
            </div>
        </div>
        </form>
        </div>
    	</ul>
    </div>
  </div> 
</div>
	

   