
<div id="search-form-div">
	<form class="form-horizontal" role="form" action="" method="POST" id="search-form">
		<div class="form-group">
			<div class="col-xs-8">
				<input type="text" class="form-control" name="searchInp" <?php if(isset($_GET["q"])) echo "value='".$_GET["q"]."'"?>>
			</div>

				<button type="submit" class="btn btn-primary" name="search">Search</button>

		</div>
	</form>
</div>
<div id="book-shelf">
	<?php
		if(isset($_GET["q"])){
			$q = "%".$_GET["q"]."%";
			$books = $myMembrs->bookSearchBetweenId($q, 0, 30);
		}
		else{
			$books = $myMembrs->selectBookBetweenId(0, 30);
		}
		
		foreach($books as $book){
			echo "<section class='book-link'>";
				echo "<a href='?Book=".$book['id']."'>";
					if(file_exists("img/cover" . $book['id'] . ".jpg")) echo "<img src='img/cover" . $book['id'] . ".jpg'>";
					else echo "<img src='img/cover0.jpg'>";
					echo "<label>" . $book['name'] . "</label>";
				echo "</a>";
			echo "</section>";
		}

	?>
</div>


